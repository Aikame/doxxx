#!/bin/bash

# vars
cal="$HOME/bin/cal.png"
back="$HOME/bin/cal_back.png"
width=$(xdotool "getdisplaygeometry" | awk '{print $1;}')
height=$(xdotool "getdisplaygeometry" | awk '{print $2;}')
xpos=$(expr $width - 150)
ypos=$(expr $height - 120)

# convert the output to png
convert -background "#1f1f1f" \
		-fill "#d3d3d3" \
		-font "cherry" \
		-antialias \
		-pointsize 12 \
		label:"$(cal)" \
		"$cal"

# fetch
n30f -x $(expr $xpos - 5) -y $(expr $ypos - 5) "$back" &
sleep ".1s"
n30f -x "$xpos" -y "$ypos" -c "pkill n30f" "$cal" &

# delete it
sleep ".2s"
rm "$cal"
