#!/bin/bash
PIC=$HOME/bin/dunst_music.png
URGENCY="normal"
# cmd
notify-send --expire-time=1200 --icon=$PIC 'Now Playing:' "$(mpc current -f "%artist% - %album%")\n$(mpc current -f %title%)"
exit 0
