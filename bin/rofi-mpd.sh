#!/usr/bin/env bash

#########################################
###  script for mpc commands in rofi  ###
### playing song from current playlist###
#########################################

#vars
mpchost="localhost"
mpcport="6600"
listformat="%title% - %album%" # recomend to not change this var
rofiprom="Music"

TITLE=$(mpc --format="$listformat" --host $mpchost --port $mpcport playlist | rofi -dmenu -p $rofiprom)
if [ "$TITLE" = "" ]; 
	then exit;fi
POS=$(mpc --format="$listformat" --host $mpchost --port $mpcport playlist | grep -nx "$TITLE" | awk -F: '{print $1}')
mpc --host $mpchost --port $mpcport play $POS
