#!/bin/bash
# taking a screenshot
scrot 'Screenshot-%Y-%m-%d-%s-$wx$h.png' -q 80 $2 -e 'mv $f $$(xdg-user-dir PICTURES)/screenshots'

# sent notify
notify-send --urgency='normal' --expire-time=2000 -i view-fullscren 'screenshot was taken, good view~' 'Saved to ~/pictures/screenshots~' --icon='/home/aikame/bin/dunst_screen.png'
