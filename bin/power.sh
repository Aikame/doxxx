#!/usr/bin/env bash
options="Lock
Restart BSPWM
Exit BSPWM
Reboot
Shutdown"

selection=$(echo -e "${options}" | rofi -dmenu -p "Power Menu")
case "${selection}" in
  "Shutdown")
    shutdown now;;
  "Reboot")
    reboot;;
  "Exit BSPWM")
    bspc quit;;
  "Restart BSPWM")
    $HOME/.config/bspwm/bspwmrc;;
  "Lock")
    betterlockscreen -l blur;;
esac
