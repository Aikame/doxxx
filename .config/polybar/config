; polybar cfg
[colors]
;background = ${xrdb:background}
background = ${colors.transparent}
foreground = ${xrdb:foreground}
black      = ${xrdb:color0}
black2     = ${xrdb:color8}
red        = ${xrdb:color1}
red2       = ${xrdb:color9}
green      = ${xrdb:color2}
green2     = ${xrdb:color10}
yellow     = ${xrdb:color3}
yellow2    = ${xrdb:color11}
blue       = ${xrdb:color4}
blue2      = ${xrdb:color12}
magenta    = ${xrdb:color5}
magenta2   = ${xrdb:color13}
cyan       = ${xrdb:color6}
cyan2      = ${xrdb:color14}
white      = ${xrdb:color7}
white2     = ${xrdb:color15}
transparent = #00000000

module-fore      = ${colors.black}

module-mpd       	 = ${colors.magenta2}
module-xkey	 	 = ${colors.yellow2}
module-xkey-indicator	 = ${colors.magenta2}
module-date      	 = ${colors.red2}
module-net	 	 = ${colors.blue2}
module-volume    	 = ${colors.green2}
module-xwin	 	 = ${colors.module-ws}
module-tray	 	 = ${colors.module-date}

module-ws	    = ${colors.magenta2}
module-ws-occupied  = ${colors.cyan}
module-ws-empty     = ${colors.white2}
module-ws-focused   = ${colors.black2} 

[section/powerline]
include-file = $HOME/.config/polybar/powerline

[bar/top1]
width    = 98%
height   = 20
offset-x = 1%
offset-y = 1%
bottom   = true
override-redirect = true
enable-ipc = true
font-0 = "cherry:antialias=true:style=medinum:pixelsize=11;2"
font-1 = "Biwidth:antialias=true:style=medinum:pixelsize=12;2"
font-2 = "Powerline Extra Symbols:style=regular:pixelsize=11;3"
font-3 = "siji:style=regular:pixelsize=7;2"

background    = ${colors.background}
foreground    = ${colors.foreground}
border-size   = 0
border-color  = ${colors.background}
line-size     = 0

modules-left   = bspwm xwin
modules-center = 
modules-right  = mpd net xkey volume date
module-margin  = 1

fixed-center   = true
wm-restack     = bspwm

tray-position = right
tray-maxsize = 16
tray-transparent = false
tray-background = ${colors.module-tray}


[module/xkey]
type = internal/xkeyboard

; List of indicators to ignore
blacklist-0 = num lock

format = <label-layout><label-indicator>
format-prefix = 
format-padding = 0
format-prefix-padding = 1
format-prefix-foreground = ${colors.module-fore}
format-prefix-background = ${colors.module-xkey}

label-layout = %layout%
label-layout-padding = 1
label-layout-background = ${colors.module-xkey}
label-layout-foreground = ${colors.module-fore}

label-indicator = %name%
label-indicator-padding = 1
label-indicator-background = ${colors.module-xkey-indicator}
label-indicator-foreground = ${colors.module-fore}

; disk
[module/disk]
type = internal/fs
mount-0 = /
interval = 10
fixed-values = true

format-mounted = <label-mounted>
label-mounted-padding = 1 
format-mounted-padding = 0

format-mounted-prefix =  
format-mounted-prefix-padding = 1
format-mounted-background = ${colors.blue}
format-mounted-prefix-foreground = ${colors.module-pref-fore}
format-mounted-prefix-background = ${colors.module-pref}

format-unmounted = <label-unmounted>
label-mounted = %used%
label-mounted-background = ${colors.module-back}
label-mounted-foreground = ${colors.module-fore}
label-unmounted = %mountpoint%: not mounted

; win
[module/xwin]
type = internal/xwindow

format = <label>
format-background = ${colors.module-xwin}
format-foreground = ${colors.module-fore}
format-padding = 0

label = %title:0:40:...%
label-padding = 1

label-empty = 
label-empty-foreground = ${colors.module-fore}

; bspwm
[module/bspwm]
type = internal/bspwm
format = <label-state> <label-mode>
format-padding = 0

index-sort = false
wrapping-scroll = false

;label-separator = " " 

label-focused            = %name%
label-focused-background = ${colors.module-ws}
label-focused-foreground = ${colors.module-ws-focused}
label-focused-padding    = 1

label-urgent            = %name%
label-urgent-background = ${colors.red}
label-urgent-foreground = ${colors.module-fore}
label-urgent-padding    = 1

label-empty            = %name%
label-empty-background = ${colors.module-ws}
label-empty-foreground = ${colors.module-ws-empty}
label-empty-padding    = 1

label-occupied            = %name%
label-occupied-background = ${colors.module-ws}
label-occupied-foreground = ${colors.module-ws-occupied}
label-occupied-padding    = 1

; Music
[module/mpd]
type = internal/mpd
host = localhost
;password = 320
port = 6600
interval = 1

format-online = <label-song>
format-online-padding = 0
format-online-prefix = 
format-online-prefix-background = ${colors.module-mpd}
format-online-prefix-foreground = ${colors.module-fore}
format-online-prefix-padding = 1

label-song = %artist% - %title%
label-song-maxlen = 40
label-song-background = ${colors.module-mpd}
label-song-foreground = ${colors.module-fore}
label-song-padding = 1

label-time = %elapsed%
label-time-background = ${colors.module-mpd}
label-time-foreground = ${colors.module-fore}
label-time-padding = 1

label-offline = mpd is offline

icon-play = 
icon-pause = 
icon-prev = 
icon-next = 

[module/net]
type = internal/network
interface = enp3s7
interval = 1.0
udspeed-minwidth = 1
accumulate-stats = false
unknown-as-up = true

format-connected = <label-connected>
format-connected-prefix = 
format-connected-prefix-padding = 1
format-connected-prefix-foreground = ${colors.module-fore}
format-connected-prefix-background = ${colors.module-net}

format-disconnected-prefix = 
format-disconnected-prefix-padding = 1
format-disconnected-prefix-foreground = ${colors.module-fore}
format-disconnected-prefix-background = ${colors.module-net}

label-connected = %downspeed:8% %upspeed:8%
label-connected-foreground = ${colors.module-fore}
label-connected-background = ${colors.module-net}
label-connected-padding = 1

label-disconnected = disconnected
label-disconnected-padding     = 1
label-disconnected-foreground = ${colors.red}
label-disconnected-background = ${colors.module-net}

[module/cpu]
type = internal/cpu
interval = 1

format = <label>
format-background = ${colors.module-back}
format-foreground = ${colors.module-fore}
format-padding = 0

format-prefix = 
format-prefix-background = ${colors.module-pref}
format-prefix-foreground = ${colors.module-pref-fore}
format-prefix-padding = 1

label = %percentage-cores%
label-padding = 1

[module/date]
type = internal/date
interval = 1.0 

time     = "%d %b/%a - %H:%M"
label             = %{A1:~/bin/cal.sh:}%time%%{A}
label-padding     = 1
label-background  = ${colors.module-date}
label-foreground  = ${colors.module-fore}

format                   = <label>
format-padding           = 0
format-prefix            = 
format-background        = ${colors.module-date}
format-foreground        = ${colors.module-fore}
format-prefix-padding    = 1 

; Volume
[module/volume]
type = internal/volume

format-volume = <label-volume>
format-volume-background = ${colors.module-volume}
format-volume-padding = 0

format-volume-prefix = 
format-volume-prefix-padding = 1
format-volume-prefix-background = ${colors.module-volume}
format-volume-prefix-foreground = ${colors.module-fore}

label-volume = %percentage%%
label-volume-background = ${colors.module-volume}
label-volume-foreground = ${colors.module-fore}
label-volume-padding = 1

format-muted = <label-muted>
format-muted-prefix = 
format-muted-prefix-padding = 1
format-muted-prefix-background = ${colors.module-volume}
format-muted-prefix-foreground = ${colors.module-fore}

label-muted = MUTED
label-muted-background = ${colors.module-volume}
label-muted-foreground = ${colors.module-fore}
label-muted-padding = 1

[global/wm]
margin-top    = 5
margin-bottom = 5
/* vim:ft=dosini
