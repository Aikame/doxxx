" vimrc
" runtime path
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

" Plugins
call plug#begin()
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'peterhoeg/vim-qml'
call plug#end()

" default settings
set exrc
set secure
set shortmess=I
" NERDTree
let NERDTreeShowHidden=1

" highlight
let base16colorspace=256
syntax on
colorscheme pablo

" lightline
set noshowmode
set laststatus=2
let g:lightline = {
      \ 'colorscheme': '16color',}
set whichwrap+=<,>,h,l,[,]

" fzf
" let g:fzf_action = {
"   \ 'ctrl-t': 'tab split',
"   \ 'ctrl-s': 'split',
"   \ 'ctrl-v': 'vsplit' }


" disable mouse 
set mouse=

" Line number
set number

" KB
map <C-n> :NERDTreeToggle<CR>
map <C-x> :q!<CR>
map <C-o> :w<CR>
