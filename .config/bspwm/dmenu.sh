#!/bin/bash

### this my try to create useful script with vars to configure dmenu
### please, use dmenu2, because original dmenu dosen't work with this script
### vars

# font
font='Sarasa Mono J'

# colors
background="#2b303b"
foreground="#c0c5ce"
sel_background="#c0c5ce"
sel_foreground="#2b303b"
lines="15" #0 - default
height="20"
width="350"
offsetx="785"
offsety="390"
### cmd
dmenu_run -fn "$font"  -nb $background -nf $foreground -sb $sel_background -sf $sel_foreground -p run -l $lines -w $width -h $height -x $offsetx -y $offsety 
