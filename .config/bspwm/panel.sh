#!/usr/bin/bash

# killing process...
killall -q polybar

# Waiting...
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# launching bar...
polybar -r top1 &
exit 0

