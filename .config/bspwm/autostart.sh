#!/bin/bash
### autostart script
### seriosly, why i don't use .xsession file??

# killing some process
pkill compton
pkill sxhkd
pkill dunst
pkill polybar

# autostart 
sxhkd &
nvidia-settings -l && nvidia-settings -a GpuPowerMizerMode=1 &
#conky -c ~/.config/conky/conkyrc
$HOME/.fehbg &
$HOME/.config/bspwm/panel.sh &
dunst &
compton --blur-method kawase --blur-strength 5 --config ~/.config/compton/compton.conf
exit 0
