#!/bin/bash
### wallpaper changes at time
HOUR=$(date +%H)
case "$HOUR" in
04|05|06|07)
feh --bg-scale $HOME/pics/wallpaper/wp-6.png
;;
08|09|10|11|12|13|14|15)
feh --bg-scale $HOME/pics/wallpaper/wp-7.png
;;
16|17|18)
feh --bg-scale $HOME/pics/wallpaper/wp-8.png
;;
*)
feh --bg-scale $HOME/pics/wallpaper/wp-9.png
;;
esac
