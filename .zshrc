### zshrc
autoload -Uz promptinit compinit up-line-or-beginning-search down-line-or-beginning-search

# if tilix
#if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#        source /etc/profile.d/vte.sh
#fi

### Window title
# %# = #
case $TERM in
  termite|*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
    precmd () { print -Pn "\e]0;%M [%~] \a" } 
    preexec () { print -Pn "\e]0;%M [%~] [$1]\a" };;
  screen|screen-256color)
    precmd () { 
      print -Pn "\e]83;title \"$1\"\a" 
      print -Pn "\e]0;$TERM - (%L) |%n@%M| [%~] :\a" 
    }
    preexec () { 
      print -Pn "\e]83;title \"$1\"\a" 
      print -Pn "\e]0;$TERM - (%L) |%n@%M| [%~] : [$1]\a" 
    };; 
esac

### fff file-manager settings
# colors
export FFF_LS_COLORS=1 		# for coloring    [0-1] (don't touch this setting)
export FFF_HIDDEN=1 		# show hidden     [0-1]
export FFF_COL1=9		# dir color       [0-9]
export FFF_COL2=4		# status color    [0-9]
export FFF_COL3=1		# selection color [0-9]
export FFF_COL4=9		# cursor color    [0-9]

# some settings
export EDITOR="nvim"		# default editor
export FFF_OPENER="xdg-open"	# file opener
export FFF_CD_ON_EXIT=1		# stay in last directory after exiting fff

export FFF_W3M_XOFFSET=0	# image preview x-offset
export FFF_W3M_YOFFSET=0	# image preview y-offset

# favourites
export FFF_FAV1=$(xdg-user-dir DESKTOP)	  # desk dir
export FFF_FAV2=$(xdg-user-dir DOWNLOAD)  # dl dir
export FFF_FAV3=$(xdg-user-dir MUSIC)	  # music dir
export FFF_FAV4=$(xdg-user-dir PICTURES)  # pics dir
export FFF_FAV5=$(xdg-user-dir VIDEOS)	  # vids dir
export FFF_FAV6=$(xdg-user-dir DOCUMENTS) # docs dir
export FFF_FAV7=$HOME/.wine/		  # wine prefix dir
export FFF_FAV8=$HOME/.config		  # .config dir
export FFF_FAV9=$HOME			  # home dir
export FFF_FILE_FORMAT="%f - %s"
export FFF_FILE_FORMAT="%f"

### extract function
extract () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2) tar xvjf $1   ;;
      *.tar.gz)  tar xvzf $1   ;;
      *.tar.xz)  tar xvfJ $1   ;;
      *.bz2)     bunzip2 $1    ;;
      *.rar)     unrar x $1    ;;
      *.gz)      gunzip $1     ;;
      *.tar)     tar xvf $1    ;;
      *.tbz2)    tar xvjf $1   ;;
      *.tgz)     tar xvzf $1   ;;
      *.zip)     unzip $1      ;;
      *.Z)       uncompress $1 ;;
      *.7z)      7z x $1       ;;
      *)         echo "'$1' cannot be extracted via extract function" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
### history
	HISTFILE=$HOME/.zsh_hist
	HISTSIZE=200
	SAVEHIST=200
	setopt append_history
	setopt extended_history
	setopt hist_expire_dups_first
	setopt hist_ignore_space
	setopt inc_append_history
	setopt share_history
### completions
	compinit -d ~/.cache/.zsh_zcompdump
	zle -N up-line-or-beginning-search
	zle -N down-line-or-beginning-search
	zstyle ':completion:*' menu select
	zstyle ':completion:*' rehash true
	#ttyctl -f
	setopt COMPLETE_ALIASES


# autocd
	setopt autocd

# syntax highlight
	source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

### keybd

# arrows [ctrl + arrow up/down]
	bindkey "^[[1;5A" up-line-or-beginning-search
	bindkey "^[[1;5B" down-line-or-beginning-search

# arrows [ctrl + </>]
	bindkey "^[[1;5C" forward-word
	bindkey "^[[1;5D" backward-word

# arrows [shift + </>]
	bindkey "^[[1;2C" delete-char
	bindkey "^[[1;2D" backward-delete-char

# arrows [shift + arrow up/down]
	bindkey "^[[1;2A" beginning-of-line
	bindkey "^[[1;2B" end-of-line

# arrows def
	bindkey "^[[A" up-line-or-search
	bindkey "^[[D" backward-char
	bindkey "^[[B" down-line-or-search
	bindkey "^[[C" forward-char
# aliases
	# pacman
	alias inst='sudo pacman -S'
	alias del='sudo pacman -Rsn'
	alias upd='sudo pacman -Syu && pakku -Su && flatpak update'
	alias fullupdpac='sudo pacman -Syyuu'
	alias packs='echo "Repo: $(pacman -Qqn | wc -l)" && echo "AUR:  $(pacman -Qqm | wc -l)" && echo  "Flatpak: $(flatpak list | wc -l)"'
	alias shitdel='sudo pacman -Scc && sudo pacman -Rsn $(pacman -Qdtq)'
	# nvim
	alias pacconf='sudo nvim /etc/pacman.conf'
	alias mirrors='sudo nvim /etc/pacman.d/mirrorlist'
	alias bsprc='nvim ~/.config/bspwm/bspwmrc'
	alias polyconf='nvim ~/.config/polybar/config'
	alias keybd='nvim ~/.config/sxhkd/sxhkdrc'
	alias nvimsu='sudo nvim'
	# other
	alias scclear='rm -rf $(xdg-user-dir PICTURES)/screenshots/*'
	alias flexx='mpv https://vk.com/video129043345_456239185'
	alias sudo='sudo '
	alias hamstart='systemctl start logmein-hamachi'
	alias hamlogin='hamachi login'
	alias hamstop='systemctl stop logmein-hamachi'
	alias fetch='./fetch && ./panes.sh'
	alias clone='git clone'

## autocorrect
setopt correctall

# greetings
	#echo 'wut ur want?'

# prompt
#promptinit
PROMPT="%m ~ "
